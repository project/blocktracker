/**
 * @file
 * Contains JavaScript for SumoLogic Logger.
 */

// Does not use behaviours since only need to run once on page load.
(function ($) {
  'use strict';

  Drupal.behaviors.blocktracker = {
    event: {
      action: 'detected',
      label: 'existing'
    },
    processResult: function (enabled) {
      // If no cookie set it's a new user.
      if (!this.cookiedata) {
        this.event.label = 'new';
      }

      /*
        If cookie is set and does not match the adBlock detection that means
        the state has changed (ad blocker got enabled or disabled).
       */
      else if (this.cookiedata !== enabled) {
        this.event.action = enabled === 'yes' ? 'turned on' : 'turned off';
      }
      else if (enabled === 'no') {
        this.event.action = 'not detected';
      }
      $.cookie('blocktracker', enabled, {
        expires: 365,
        path: '/'
      });
      if (typeof ga === 'function') {
        ga('send', 'event', 'adblock', this.event.action, this.event.label, {
          nonInteraction: 1
        });
      }
      // Trigger an event with the data so other modules or scripts can act.
      this.event.enabled = enabled;
      $(window).trigger('blocktracker', this.event);
    },
    attach: function (context) {
      var self = this;
      self.cookiedata = $.cookie('blocktracker');
      $('body', context).once('blocktracker', function () {
        if (typeof blockAdBlock === 'undefined') {
          self.processResult('yes');
        }
        else {
          blockAdBlock.onDetected(function () {
            self.processResult('yes');
          });
          blockAdBlock.onNotDetected(function () {
            self.processResult('no');
          });
        }
      });
    }
  };
})(jQuery);
